# liboxyd
## Description

This is the client-side implementation of the cryptographic stack of Coldwire in Rust for WASM.
* ecies with the secp256k1 curve for key and nonce encryption
* XChaCha20-Poly305 for large files encryption

## Install

Before development, make sure you have installed:
- [wasm-pack](https://github.com/rustwasm/wasm-pack)
- [cargo-watch](https://github.com/watchexec/cargo-watch)
- node 16.0.0

```bash
# clone the repo
git clone https://git.coldwire.org/coldwire/libraries/liboxyd.git
cd liboxyd

# install dependencies
yarn

# start development
yarn run dev

# run example
yarn run example

# visit http://localhost:8080
```

After installation, start developing here:
- `./core`: Rust source files
- `./src`: TypeScript source files

## Structure

- `./core`: Rust code, the core of the library
- `./pkg`: `wasm-pack` built npm-ready library
- `./src`: TypeScript source code
- `./example`: A web app that to test liboxyd`

## License

Based on [Rust TypeScript Template](https://github.com/iantheearl/rust-typescript-template)
