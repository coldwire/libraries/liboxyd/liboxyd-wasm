import { hash as native_hash, hkdf as native_hkdf } from "../../pkg";

export const hash = (data: Uint8Array): Uint8Array => {
  return native_hash(data);
};

export const hkdf = (password: string): Uint8Array => {
  return native_hkdf(password);
};
