import { ChaCha20 } from "../../pkg";
import { Writer, Reader } from "../types/Stream";
import {
  XChaCha20,
  EncryptResultSync,
  DecryptResultSync,
  DecryptResultStream,
} from "../types/Xchacha20";

// Synchrone encryption ------------------------------------

export class sync implements XChaCha20 {
  data: Uint8Array | undefined;
  key: Uint8Array | undefined;

  constructor(data: Uint8Array, key?: Uint8Array) {
    this.data = data;
    !key ? (this.key = ChaCha20.generate_key()) : (this.key = key);
  }

  async encrypt(): Promise<EncryptResultSync> {
    if (this.key && this.data) {
      const out = await ChaCha20.encrypt_sync(this.data, this.key);

      return {
        key: this.key,
        out: out,
      };
    } else {
      throw new Error("Please set a data source");
    }
  }

  async decrypt(): Promise<DecryptResultSync> {
    if (this.key && this.data) {
      const out = await ChaCha20.decrypt_sync(this.data, this.key);
      return out;
    } else {
      throw new Error("Please set a key a a data source");
    }
  }
}

// Stream encryption ---------------------------------------

export class stream implements XChaCha20 {
  src: Reader | undefined;
  out: Writer | undefined;

  key: Uint8Array | undefined;
  nonce: Uint8Array | undefined;

  constructor(src: Reader, out: Writer, key?: Uint8Array, nonce?: Uint8Array) {
    this.src = src;
    this.out = out;

    !key ? (this.key = ChaCha20.generate_key()) : (this.key = key);
    !nonce
      ? (this.nonce = ChaCha20.generate_stream_none())
      : (this.nonce = nonce);
  }

  encrypt(): Promise<void> {
    if (this.nonce && this.key && this && this.out) {
      return ChaCha20.encrypt_stream(this.nonce, this.key, this.src, this.out);
    } else {
      throw new Error("Please set input and output streams");
    }
  }
  decrypt(): Promise<DecryptResultStream> {
    throw new Error("Please set input and output streams");
  }
}
