import { KEYS, ECDSA, ECIES } from "../../pkg";
import { base64 } from "../types/Base64";
import { KeyPair, SecretKey, Phrase, PublicKey } from "../types/Keys";

export class Keypair {
  keypair: KeyPair | undefined;

  constructor(keypair?: KeyPair) {
    if (keypair) this.keypair = keypair;
  }

  import_from_sk(sk: SecretKey): void {
    const pk = KEYS.pk_from_sk(sk);

    this.keypair = {
      SecretKey: sk,
      PublicKey: pk,
    };
  }

  import_from_phrase(phrase: Phrase): void {
    const sk = KEYS.phrase_to_sk(phrase.join(" "));
    this.import_from_sk(sk);
  }

  export_to_phrase(): Phrase {
    if (this.keypair) {
      const out_phrase = KEYS.sk_to_phrase(this.keypair.SecretKey).split(" ");
      return out_phrase;
    }

    throw new Error("No keypair");
  }

  export_public_key(): PublicKey | undefined {
    if (this.keypair?.PublicKey) {
      return this.keypair.PublicKey;
    }
  }

  export(): KeyPair | undefined {
    return this.keypair;
  }

  generate(): KeyPair {
    const [sk, pk] = KEYS.generate_key_pair();
    const res = {
      SecretKey: sk,
      PublicKey: pk,
    };

    this.keypair = res;
    return this.keypair;
  }
}

export class Ecdsa extends Keypair {
  sign(data: Uint8Array): base64 {
    if (this.keypair && data) {
      return ECDSA.sign(this.keypair?.SecretKey, data);
    }

    throw new Error("No keypair or data");
  }

  verify(signature: base64, data: Uint8Array): boolean {
    if (this.keypair && data) {
      return ECDSA.verify(this.keypair?.PublicKey, data, signature);
    }

    throw new Error("No keypair or data");
  }
}

export class Ecies extends Keypair {
  async encrypt(data: Uint8Array): Promise<Uint8Array> {
    if (this.keypair && data) {
      return await ECIES.encrypt(this.keypair?.PublicKey, data);
    }

    throw new Error("No keypair or data");
  }

  async decrypt(data: Uint8Array): Promise<Uint8Array> {
    if (this.keypair && data) {
      return await ECIES.decrypt(this.keypair?.SecretKey, data);
    }

    throw new Error("No keypair or data");
  }
}
