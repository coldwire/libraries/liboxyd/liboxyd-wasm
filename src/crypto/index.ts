import * as secp256k1 from "./secp256k1";
import * as sha256 from "./sha256";
import * as xchacha20 from "./xchacha20";

export const crypto = {
  secp256k1,
  sha256,
  xchacha20,
};
