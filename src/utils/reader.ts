import { Reader, ReaderResult, SourceStreamResult } from "../types/Stream";
export class SourceStream implements Reader {
  file: File | Blob;
  chunkSize: number;

  chunkId = 0;
  offset = 0;

  constructor(file: File | Blob, chunkSize = 65536) {
    this.file = file;
    this.chunkSize = chunkSize;
  }

  public read(): Promise<SourceStreamResult> {
    return new Promise((resolve) => {
      if (this.offset >= this.file.size) {
        resolve({
          done: true,
          chunk: 0,
          value: new Uint8Array(), // Empty uint array
        });
      } else {
        const chunk = this.file.slice(
          this.offset,
          this.chunkSize + this.offset
        );

        this.offset += this.chunkSize;

        chunk.arrayBuffer().then((value) => {
          if (chunk.size < this.chunkSize) {
            resolve({
              done: true,
              chunk: this.chunkId,
              value: new Uint8Array(value),
            });
          } else {
            resolve({
              done: false,
              chunk: this.chunkId,
              value: new Uint8Array(value),
            });
          }
        });

        this.chunkId++;
      }
    });
  }
}

export class DownloadStream implements Reader {
  endpoint: string;
  chunks = 0;
  part = 1;

  constructor(endpoint: string, chunks: number) {
    this.endpoint = endpoint;
    this.chunks = chunks; // Number of chunks
  }

  public read(): Promise<ReaderResult> {
    return new Promise((resolve) => {
      if (this.part > this.chunks) {
        resolve({
          done: true,
          value: new Uint8Array(),
        });
      } else {
        fetch(`${this.endpoint}/${this.part}`)
          .then((res) => res.blob())
          .then((blob) => {
            blob.arrayBuffer().then((value) => {
              if (this.part == this.chunks) {
                resolve({
                  done: true,
                  value: new Uint8Array(value),
                });
              } else {
                resolve({
                  done: false,
                  value: new Uint8Array(value),
                });
              }

              this.part++;
            });
          });
      }
    });
  }
}
