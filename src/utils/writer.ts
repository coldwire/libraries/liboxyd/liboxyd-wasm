import { API_ENDPOINT } from "..";
import {
  ApiResponse,
  ApiResponseUploadCloseSuccess,
  ApiRequestUploadStop,
} from "../types/Api";
import { Writer } from "../types/Stream";

type CloseCallback = (res: ApiResponse<ApiResponseUploadCloseSuccess>) => void;

export class UploadWriter implements Writer {
  closed = false;
  part = 0;

  uid: string;
  metadata: ApiRequestUploadStop | undefined;
  callback: CloseCallback;

  constructor(uid: string, callback: CloseCallback) {
    this.uid = uid;
    this.callback = callback;
  }

  setMetadata(meta: ApiRequestUploadStop) {
    this.metadata = meta;
  }

  public write(chunk: Uint8Array, part: number): Promise<void> {
    return new Promise((resolve, reject) => {
      fetch(`${API_ENDPOINT}/blocs/upload/${this.uid}/${part}`, {
        method: "PUT",
        body: chunk,
      }).then((res) => {
        if (res.ok) {
          resolve();
        } else {
          this.abort().finally(() => reject(res));
        }
      });
    });
  }
  public close(): Promise<ApiResponse<ApiResponseUploadCloseSuccess>> {
    return new Promise((reject) => {
      fetch(`${API_ENDPOINT}/blocs/upload/stop/${this.uid}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(this.metadata),
      })
        .then((res) => res.json())
        .then((res: ApiResponse<ApiResponseUploadCloseSuccess>) => {
          if (res.status === "SUCCESS") {
            this.callback(res);
          } else {
            this.abort().finally(() => reject(res));
          }
        })
        .catch((e) => reject(e));
    });
  }
  public abort(): Promise<void> {
    return new Promise((resolve, reject) => {
      fetch(`${API_ENDPOINT}/blocs/upload/abort/${this.uid}`, {
        method: "POST",
      })
        .then(() => resolve())
        .catch((e) => reject(e));
    });
  }
}
