import { base64_encode, base64_decode } from "../../pkg";
import { base64 } from "../types/Base64";

export default {
  encode(data: Uint8Array): base64 {
    return base64_encode(data);
  },

  decode(data: base64): Uint8Array {
    return base64_decode(data);
  },
};
