export default (size: number): Uint8Array => {
  const res = new Uint8Array(size);
  return window.crypto.getRandomValues(res);
};
