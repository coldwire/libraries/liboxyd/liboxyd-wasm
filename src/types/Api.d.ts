export interface ApiResponse<T> {
  status: string;
  message: string;
  content: T;
}

export type ApiResponseAuthSucces = {
  username: string;
  token_expiration: number;
  public_key: string;
  root: string;
};

export type ApiResponseUploadCloseSuccess = {
  id: string;
  name: string;
  parent: string;
  owner: string;
  size: number;
  mime_type: string;
  is_favorite: boolean;
};

export type ApiResponseUploadStartSuccess = {
  id: string;
};

export type ApiRequestUploadStop = {
  mime_type: string;
  key: string;
  nonce: string;
};
