interface EncryptResult {
  key: Uint8Array;
}

export interface EncryptResultSync extends EncryptResult {
  out: Uint8Array;
}

export interface EncryptResultStream extends EncryptResult {
  nonce: Uint8Array;
}

export type DecryptResultSync = Uint8Array;
export type DecryptResultStream = void;

export interface XChaCha20 {
  key: Uint8Array | undefined;

  encrypt(): Promise<EncryptResult | void>;
  decrypt(): Promise<DecryptResultSync | DecryptResultStream>;
}
