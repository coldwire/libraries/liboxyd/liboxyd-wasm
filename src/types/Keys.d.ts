export type SecretKey = string;
export type PublicKey = string;
export type Phrase = Array<string>;

export type KeyPair = {
  SecretKey: SecretKey;
  PublicKey: PublicKey;
};
