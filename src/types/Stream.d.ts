export interface Reader {
  read(): Promise<ReaderResult>;
}

export interface ReaderResult {
  done: boolean;
  value: Uint8Array;
}

export interface SourceStreamResult extends ReaderResult {
  chunk: number;
}

export interface Writer {
  closed: boolean;
  write(chunk: Uint8Array, part: number): void;
  close(): void;
  abort(): void;
}
