import { API_ENDPOINT } from "..";
import { crypto } from "../crypto";
import { Ecies } from "../crypto/secp256k1";
import {
  ApiResponse,
  ApiResponseUploadCloseSuccess,
  ApiResponseUploadStartSuccess,
} from "../types/Api";
import { writer, reader, base64 } from "../utils";

import { keys } from ".";

export const upload = (
  file: File,
  parent: string
): Promise<ApiResponse<ApiResponseUploadStartSuccess>> => {
  return new Promise((resolve, reject) => {
    if (
      keys.secretKey.isLoaded() === keys.LoaderErrors.UNLOADED ||
      keys.secretKey.isLoaded() === keys.LoaderErrors.NULL
    ) {
      reject(keys.secretKey.isLoaded());
    } else {
      fetch(`${API_ENDPOINT}/blocs/upload/start`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          name: file.name,
          parent: parent,
        }),
      })
        .then((res) => res.json())
        .then(async (res: ApiResponse<ApiResponseUploadStartSuccess>) => {
          if (res.status === "SUCCESS") {
            let response:
              | ApiResponse<ApiResponseUploadCloseSuccess>
              | undefined;

            // Upload each chunk and resolve when everything is done
            const uploader = new writer.UploadWriter(
              res.content.id,
              (res) => (response = res)
            );

            // Read file chunk by chunk
            const source = new reader.SourceStream(file);

            // Init XChaCha20
            const xchacha = new crypto.xchacha20.stream(source, uploader);

            // Encrypt key and nonce
            const sk = await keys.secretKey.get();
            const keysCrypt = new Ecies();
            keysCrypt.import_from_sk(sk);

            if (xchacha.key && xchacha.nonce) {
              const encryptedKeyUint = await keysCrypt.encrypt(xchacha.key);
              const encryptedNonceUint = await keysCrypt.encrypt(xchacha.nonce);

              const encryptedKey = base64.encode(encryptedKeyUint);
              const encryptedNonce = base64.encode(encryptedNonceUint);

              uploader.setMetadata({
                key: encryptedKey,
                nonce: encryptedNonce,
                mime_type: file.type,
              });
            } else {
              uploader.abort().finally(() => reject("aborted"));
            }

            // Start upload
            xchacha
              .encrypt()
              .then(() => {
                console.log(response);
                response
                  ? resolve(response)
                  : uploader.abort().finally(() => reject("aborted"));
              })
              .catch(() => uploader.abort().finally(() => reject("aborted")))
              .finally(() => console.log("aaaaaaaaaaaaaaaaaaaa"));
          } else {
            reject(res.message);
          }
        })
        .catch((e) => reject(e));
    }
  });
};

// export const download = () => { };
