import { API_ENDPOINT } from "..";
import { KEYS } from "../../pkg";
import { Ecdsa, Keypair } from "../crypto/secp256k1";
import { ApiResponse, ApiResponseAuthSucces } from "../types/Api";
import { Phrase, SecretKey } from "../types/Keys";
import base64 from "../utils/base64";
import random from "../utils/random";

import { keys as keyManager } from ".";

/* ===================== UTILS ===================== */

const storeSecretKey = (phrase: Phrase, password?: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    keyManager.secretKey
      .tempLoad(phrase)
      .then(() => {
        if (password) {
          keyManager.secretKey
            .store(password, phrase)
            .then(() => resolve())
            .catch((e: unknown) => reject(e));
        } else {
          resolve();
        }
      })
      .catch((e: unknown) => reject(e));
  });
};

const storePublicKey = (
  secret: SecretKey,
  password?: string
): Promise<void> => {
  return new Promise((resolve, reject) => {
    const pk = KEYS.pk_from_sk(secret);

    keyManager.publicKey
      .tempLoad(pk)
      .then(() => {
        if (password) {
          keyManager.publicKey
            .store(pk)
            .then(() => resolve())
            .catch((e) => reject(e));
        }
      })
      .catch((e: unknown) => reject(e));
  });
};

// After login/register request
const postAuth = (phrase: Phrase, password?: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    storeSecretKey(phrase, password).then(() => {
      if (password) {
        keyManager.secretKey.get().then((sk) => {
          storePublicKey(sk, password)
            .then(() => resolve())
            .catch((e) => reject(e));
        });
      } else {
        resolve();
      }
    });
  });
};

/* ===================== API ===================== */

export const new_account = (): Phrase => {
  const keys = new Keypair();
  keys.generate();
  return keys.export_to_phrase();
};

export const register = (
  phrase: Phrase,
  username: string,
  password?: string
): Promise<ApiResponse<ApiResponseAuthSucces>> => {
  return new Promise((resolve, reject) => {
    const ecdsa = new Ecdsa();
    ecdsa.import_from_phrase(phrase);
    const publicKey = ecdsa.export_public_key();

    if (publicKey) {
      const decodedPublicKey = base64.decode(publicKey);
      const signedPublicKey = ecdsa.sign(decodedPublicKey);

      fetch(`${API_ENDPOINT}/auth/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          public_key: publicKey,
          signed_public_key: signedPublicKey,
          username: username,
        }),
      })
        .then((res) => res.json())
        .then((res) => {
          if (res.status === "SUCCESS") {
            postAuth(phrase, password)
              .then(() => resolve(res))
              .catch((e) => reject(e));
          } else {
            reject(res.message);
          }
        });
    } else {
      reject();
    }
  });
};

export const login = (
  phrase: Phrase,
  username: string,
  password?: string
): Promise<ApiResponse<ApiResponseAuthSucces>> => {
  return new Promise((resolve, reject) => {
    const challenge = random(64);

    const ecdsa = new Ecdsa();
    ecdsa.import_from_phrase(phrase);

    const signedChallenge = ecdsa.sign(challenge);

    fetch(`${API_ENDPOINT}/auth/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        challenge: base64.encode(challenge),
        signed_challenge: signedChallenge,
        username: username,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.status === "SUCCESS") {
          postAuth(phrase, password)
            .then(() => resolve(res))
            .catch((e) => reject(e));
        } else {
          reject(res.message);
        }
      });
  });
};
