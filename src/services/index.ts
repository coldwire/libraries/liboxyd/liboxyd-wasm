import * as auth_mod from "./auth";
import * as files_mod from "./files";
import * as keys_mod from "./keys";

export const auth = auth_mod;
export const files = files_mod;
export const keys = keys_mod;
