import { KEYS } from "../../pkg";
import { crypto } from "../crypto";
import { Phrase, PublicKey } from "../types/Keys";
import base64 from "../utils/base64";

const STORAGE_SECRET_KEY = "secret_key";
const STORAGE_PUBLIC_KEY = "public_key";

let SECRET_KEY: string | undefined;
let PUBLIC_KEY: string | undefined;

export enum LoaderErrors {
  LOADED = "LOADED", // Key is loaded
  UNLOADED = "UNLOADED", // Key is not loaded but is in local storage
  NULL = "NULL", // Key is not load and not in local storage
}

export const secretKey = {
  // Check if secret key is loaded or exist in local storage
  isLoaded(): LoaderErrors {
    if (!SECRET_KEY) {
      if (localStorage.getItem(STORAGE_SECRET_KEY) === null) {
        return LoaderErrors.NULL;
      } else {
        return LoaderErrors.UNLOADED;
      }
    } else {
      return LoaderErrors.LOADED;
    }
  },

  // Get loaded secret key
  get(): Promise<string> {
    return new Promise((resolve, reject) => {
      if (SECRET_KEY && this.isLoaded() === LoaderErrors.LOADED) {
        resolve(SECRET_KEY);
      } else {
        reject(this.isLoaded());
      }
    });
  },

  // Encrypt and store secret key in local storage
  store(password: string, phrase: Phrase): Promise<void> {
    return new Promise((resolve, reject) => {
      if (password.length >= 8) {
        const passwordKdf = crypto.sha256.hkdf(password);

        if (phrase.length === 24) {
          const secretKey = KEYS.phrase_to_sk(phrase.join(" "));
          const decodedSecretKey = base64.decode(secretKey);

          const chacha = new crypto.xchacha20.sync(
            decodedSecretKey,
            passwordKdf
          );
          chacha
            .encrypt()
            .then((res) => {
              const encodedEncryptedSecretKey = base64.encode(res.out);

              localStorage.setItem(
                STORAGE_SECRET_KEY,
                encodedEncryptedSecretKey
              );

              this.isLoaded() === LoaderErrors.UNLOADED || LoaderErrors.LOADED
                ? resolve()
                : reject(this.isLoaded());
            })
            .catch((e) => reject(e));
        }
      } else {
        reject();
      }
    });
  },

  // Load encrypted secret key from local storage
  load(password: string): Promise<void> {
    return new Promise((resolve, reject) => {
      if (password.length >= 8) {
        const passwordKdf = crypto.sha256.hkdf(password);

        const encryptedKey = localStorage.getItem(STORAGE_SECRET_KEY);

        if (encryptedKey) {
          const EncryptedKey = base64.decode(encryptedKey);

          const chacha = new crypto.xchacha20.sync(EncryptedKey, passwordKdf);
          chacha
            .decrypt()
            .then((secretKey) => {
              const encodedSecretKey = base64.encode(secretKey);

              SECRET_KEY = encodedSecretKey;

              this.isLoaded() === LoaderErrors.LOADED
                ? resolve()
                : reject(this.isLoaded());
            })
            .catch((e) => reject(e));
        }
      } else {
        reject();
      }
    });
  },

  // Load key without needing it to be in local storage
  tempLoad(phrase: Phrase): Promise<void> {
    return new Promise((resolve, reject) => {
      if (phrase.length === 24) {
        const secretKey = KEYS.phrase_to_sk(phrase.join(" "));

        SECRET_KEY = secretKey;

        this.isLoaded() === LoaderErrors.LOADED
          ? resolve()
          : reject(this.isLoaded());
      } else {
        reject();
      }
    });
  },
};

export const publicKey = {
  // Check if public key is loaded or exist in local storage
  isLoaded(): LoaderErrors {
    if (!PUBLIC_KEY) {
      if (localStorage.getItem(STORAGE_PUBLIC_KEY) === null) {
        return LoaderErrors.NULL;
      } else {
        return LoaderErrors.UNLOADED;
      }
    } else {
      return LoaderErrors.LOADED;
    }
  },

  // Get loaded public key
  get(): Promise<string> {
    return new Promise((resolve, reject) => {
      if (PUBLIC_KEY && this.isLoaded() === LoaderErrors.LOADED) {
        resolve(PUBLIC_KEY);
      } else {
        reject(this.isLoaded());
      }
    });
  },

  // Store public key
  store(publicKey: PublicKey): Promise<void> {
    return new Promise((resolve, reject) => {
      localStorage.setItem(STORAGE_PUBLIC_KEY, publicKey);

      this.isLoaded() === LoaderErrors.UNLOADED || LoaderErrors.LOADED
        ? resolve()
        : reject(this.isLoaded());
    });
  },

  // Load public key from local storage
  load(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.isLoaded() === LoaderErrors.UNLOADED || LoaderErrors.LOADED) {
        const pk = localStorage.getItem(STORAGE_PUBLIC_KEY);

        if (pk) {
          PUBLIC_KEY = pk;

          this.isLoaded() === LoaderErrors.LOADED
            ? resolve()
            : reject(this.isLoaded());
        } else {
          reject();
        }
      } else {
        reject(this.isLoaded());
      }
    });
  },

  // Load key without needing it to be in local storage
  tempLoad(publicKey: PublicKey): Promise<void> {
    return new Promise((resolve, reject) => {
      PUBLIC_KEY = publicKey;

      this.isLoaded() === LoaderErrors.LOADED
        ? resolve()
        : reject(this.isLoaded());
    });
  },
};
