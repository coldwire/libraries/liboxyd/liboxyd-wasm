import { InitOutput } from "../pkg";

import { crypto as crypto_mod } from "./crypto";
import * as services_mod from "./services";
import * as utils_mod from "./utils";
import { init as wasm_init } from "./wasm";

export let API_ENDPOINT: string;

export const init = (endpoint: string): Promise<InitOutput> => {
  API_ENDPOINT = endpoint;
  return wasm_init();
};
export const crypto = crypto_mod;
export const utils = utils_mod;
export const services = services_mod;
