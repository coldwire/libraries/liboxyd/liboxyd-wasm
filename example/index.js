import { init, crypto, utils, services } from "../";

window.liboxyd = {
  crypto,
  utils,
  services,
};

// FORMS

const key_field = document.getElementById("key");
const username_field = document.getElementById("username");
const root_field = document.getElementById("root");
const file_id = document.getElementById("fileid");

const login_username_field = document.getElementById("login-username");
const login_phrase_field = document.getElementById("login-phrase");
const login_password_field = document.getElementById("login-password");
const login_submit_button = document.getElementById("login-btn");

login_submit_button.addEventListener("click", async () => {
  console.log(
    login_phrase_field.value,
    login_username_field.value,
    login_password_field.value
  );

  const res = await services.auth.login(
    login_phrase_field.value.split(" "),
    login_username_field.value,
    login_password_field.value
  );

  key_field.innerText = `${res.content.public_key.slice(0, 32)}[...]`;
  username_field.innerText = res.content.username;
  root_field.innerText = res.content.root;
});

const register_username_field = document.getElementById("register-username");
const register_phrase_field = document.getElementById("register-phrase");
const register_password_field = document.getElementById("register-password");
const register_submit_button = document.getElementById("register-btn");

register_submit_button.addEventListener("click", async () => {
  console.log(
    register_phrase_field.value,
    register_username_field.value,
    register_password_field.value
  );

  const res = await services.auth.register(
    register_phrase_field.value.split(" "),
    register_username_field.value,
    register_password_field.value
  );

  key_field.innerText = `${res.content.public_key.slice(0, 32)}[...]`;
  username_field.innerText = res.content.username;
  root_field.innerText = res.content.root;
});

const upload_file_field = document.getElementById("upload-file");
const upload_submit_button = document.getElementById("upload-btn");

upload_submit_button.addEventListener("click", () => {
  console.log(upload_file_field.files[0], root_field.innerText);

  services.files
    .upload(upload_file_field.files[0], root_field.innerText)
    .then((res) => console.log(res))
    .catch((e) => console.error(e))
    .finally(() => console.log("meoww ???????"));

  // console.log(res);
  // file_id.innerText = res.content.id;
});

// INIT LIB / TEST CRYPTO

const TEST_ENDPONT = "http://coldwire.test/api";

init(TEST_ENDPONT).then(() => {
  const data = window.crypto.getRandomValues(new Uint8Array(32)); // test data

  // Create keypar
  console.log("------------- KEY PAIR -------------");
  const keys = new crypto.secp256k1.Keypair();
  keys.generate();
  const phrase = keys.export_to_phrase();

  register_phrase_field.value = phrase.join(" ");

  console.log("(liboxyd): test > key pair", keys.export());
  console.log("(liboxyd): test > phrase", phrase);

  const keysFromPhrase = new crypto.secp256k1.Keypair();
  keysFromPhrase.import_from_phrase(phrase);
  console.log("(liboxyd): test > key pair from phrase", keys.export());

  // ECDSA TESTS
  console.log("------------- ECDSA -------------");
  const ecdsa = new crypto.secp256k1.Ecdsa(keys.export());

  console.log("(liboxyd): test > data to sign", utils.base64.encode(data));
  const sign = ecdsa.sign(data);

  console.log("(liboxyd): test > signed data", sign);

  const verify = ecdsa.verify(sign, data);
  console.log("(liboxyd): test > is signature valid", verify);

  console.log("------------- ECIES -------------");
  const ecies = new crypto.secp256k1.Ecies(keys.export());
  console.log("(liboxyd): test > data to encrypt", data);

  ecies.encrypt(data).then((encrypted) => {
    console.log("(liboxyd): test > encrypted data", encrypted);
    ecies
      .decrypt(encrypted)
      .then((decrypted) =>
        console.log("(liboxyd): test > decrypted data", decrypted)
      );
  });
});

// window.oxydCrypto = crypto;

// const test_encrypt = (file) => {
//   const chacha = new crypto.chacha20();
//   chacha.generate_stream_nonce();
//   chacha.import_key_from_password("passsswoooord").then(() => {
//     console.log(chacha.nonce, chacha.key);

//     const reader = new utils.stream.SourceStream(file);
//     const writer = new utils.stream.WriteSteam({
//       async write(chunk, id) {
//         console.log(chunk, id);
//       },

//       async close() {
//         console.log("close");
//       },

//       async abort() {
//         console.log("abort");
//       },
//     });

//     chacha
//       .stream_encrypt(reader, writer)
//       .then(() => console.log("done!"))
//       .catch((e) => console.log(e));
//   });
// };

// const file = document.getElementById("file");
// const upload = document.getElementById("upload");

// upload.addEventListener("click", () => {
//   if (file.files[0]) {
//     test_encrypt(file.files[0]);
//   } else {
//     console.error("No file selected !");
//   }
// });
