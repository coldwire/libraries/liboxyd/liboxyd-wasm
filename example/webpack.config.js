const path = require("path");

const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  devServer: {
    allowedHosts: "all",
  },
  entry: "./index.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "index.js",
  },
  mode: process.env.NODE_ENV || "development",
  plugins: [new CopyWebpackPlugin({ patterns: ["index.html", "style.css"] })],
  stats: "errors-only",
};
