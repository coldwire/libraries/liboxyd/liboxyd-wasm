use js_sys::Uint8Array;
use wasm_bindgen::{prelude::wasm_bindgen, JsError};

use liboxyd::sha256::{hash as native_hash, hkdf as native_hkdf};

#[wasm_bindgen]
pub fn hash(data: &[u8]) -> Uint8Array {
    let h = native_hash(data);
    Uint8Array::from(&h[..])
}

#[wasm_bindgen]
pub fn hkdf(password: String) -> Result<Uint8Array, JsError> {
    match native_hkdf(password.as_bytes()) {
        Ok(kdf) => Ok(Uint8Array::from(&kdf[..])),
        Err(e) => Err(JsError::new(&e.to_string())),
    }
}

