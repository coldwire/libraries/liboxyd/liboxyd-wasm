use liboxyd::{secp256k1::keys};

use js_sys::{Array, JsString};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct KEYS;

#[wasm_bindgen]
impl KEYS {
    pub fn generate_key_pair() -> Array {
        let (sk, pk) = keys::generate_keypair();

        let (sk, pk) = (JsString::from(sk), JsString::from(pk));

        let res = Array::new();
        res.push(&sk);
        res.push(&pk);
        res
    }

    pub fn sk_to_phrase(sk: String) -> Result<JsString, JsError> {
        match keys::to_phrase(sk) {
            Ok(p) => Ok(JsString::from(p)),
            Err(e) => Err(JsError::new(&e.to_string())),
        }
    }

    pub fn phrase_to_sk(phrase: String) -> Result<JsString, JsError> {
        match keys::from_phrase(phrase) {
            Ok(k) => Ok(JsString::from(k)),
            Err(e) => return Err(JsError::new(&e.to_string())),
        }        
    }

    pub fn pk_from_sk(sk: String) -> Result<JsString, JsError> {
        match keys::pk_from_sk(sk) {
            Ok(k) => Ok(JsString::from(k)),
            Err(err) => Err(JsError::new(&err.to_string())),
        }
    }
}
