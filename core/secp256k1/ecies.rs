use js_sys::Uint8Array;
use wasm_bindgen::prelude::*;

use liboxyd::secp256k1::ecies::crypto::{decrypt as lib_decrypt, encrypt as lib_encrypt};

#[wasm_bindgen]
pub struct ECIES;

#[wasm_bindgen]
impl ECIES {
    pub async fn encrypt(pk: String, data: &[u8]) -> Result<Uint8Array, JsError> {
        match lib_encrypt(pk, data).await {
            Ok(res) => Ok(Uint8Array::from(&res[..])),
            Err(e) => Err(JsError::new(&e.to_string())),
        }
    }

    pub async fn decrypt(sk: String, data: &[u8]) -> Result<Uint8Array, JsError> {
        match lib_decrypt(sk, data).await {
            Ok(res) => Ok(Uint8Array::from(&res[..])),
            Err(e) => Err(JsError::new(&e.to_string())),
        }
    }
}
