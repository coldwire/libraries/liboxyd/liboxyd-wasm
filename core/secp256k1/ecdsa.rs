use js_sys::{Boolean, JsString};
use wasm_bindgen::prelude::*;

use liboxyd::secp256k1::ecdsa::{sign as secp256k1_sign, verify as secp256k1_verify};

#[wasm_bindgen]
pub struct ECDSA;

#[wasm_bindgen]
impl ECDSA {
    pub fn sign(sk: String, data: &[u8]) -> Result<JsString, JsError> {
        match secp256k1_sign(sk, data) {
            Ok(s) => Ok(JsString::from(s)),
            Err(e) => Err(JsError::new(&e.to_string())),
        }
    }

    pub fn verify(pk: String, data: &[u8], signature: String) -> Result<Boolean, JsError> {
        match secp256k1_verify(pk, signature, data) {
            Ok(res) => Ok(Boolean::from(res)),
            Err(e) => Err(JsError::new(&e.to_string())),
        }
    }
}
