use js_sys::{JsString, Uint8Array};
use wasm_bindgen::prelude::*;

use liboxyd::utils::base64;

#[wasm_bindgen]
pub fn base64_encode(data: &[u8]) -> JsString {
    JsString::from(base64::encode(data))
}

#[wasm_bindgen]
pub fn base64_decode(data: &str) -> Result<Uint8Array, JsError> {
    match base64::decode(data) {
        Ok(d) => Ok(Uint8Array::from(d.as_slice())),
        Err(e) => return Err(JsError::new(&e.to_string())),
    }
}
