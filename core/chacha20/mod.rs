pub mod stream;

use chacha20poly1305::aead::{OsRng, rand_core::RngCore};
use js_sys::Uint8Array;

use liboxyd::chacha20::{
    sync::{decrypt as decrypt_sync, encrypt as encrypt_sync},
    utils::{generate_key, generate_nonce, CHACHA_STREAM_NONCE_LENGTH},
};
use stream::{decrypt_stream as _decrypt_stream, encrypt_stream as _encrypt_stream};

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    pub type ReadStream;

    #[wasm_bindgen(catch, method)]
    async fn read(this: &ReadStream) -> Result<JsValue, JsValue>;
}

#[wasm_bindgen]
extern "C" {
    pub type OutputStream;

    #[wasm_bindgen(catch, method)]
    async fn write(
        this: &OutputStream,
        output: &[u8],
        chunk: Option<i32>,
    ) -> Result<JsValue, JsValue>;

    #[wasm_bindgen(catch, method)]
    async fn close(this: &OutputStream) -> Result<JsValue, JsValue>;
}

#[wasm_bindgen]
pub struct ChaCha20;

#[wasm_bindgen]
impl ChaCha20 {
    pub async fn encrypt_stream(
        nonce: &[u8],
        key: &[u8],
        src: ReadStream,
        out: OutputStream,
    ) -> Result<(), JsValue> {
        _encrypt_stream(nonce, key, src, out).await
    }

    pub async fn decrypt_stream(
        nonce: &[u8],
        key: &[u8],
        src: ReadStream,
        out: OutputStream,
    ) -> Result<(), JsValue> {
        _decrypt_stream(nonce, key, src, out).await
    }

    pub async fn encrypt_sync(data: &[u8], key: &[u8]) -> Result<Uint8Array, JsError> {
        let ciphertext = encrypt_sync(data, key).await;

        match ciphertext {
            Ok(v) => Ok(Uint8Array::from(&v[..])),
            Err(e) => Err(JsError::new(&e)),
        }
    }

    pub async fn decrypt_sync(data: &[u8], key: &[u8]) -> Result<Uint8Array, JsError> {
        let plaintext = decrypt_sync(data, key).await;

        match plaintext {
            Ok(v) => Ok(Uint8Array::from(&v[..])),
            Err(e) => Err(JsError::new(&e)),
        }
    }

    pub fn generate_key() -> Uint8Array {
        let key = generate_key();
        Uint8Array::from(key.as_slice())
    }

    pub fn generate_nonce() -> Uint8Array {
        let key = generate_nonce();
        Uint8Array::from(key.as_slice())
    }

    pub fn generate_stream_none() -> Uint8Array {
      let mut buf = [0u8; CHACHA_STREAM_NONCE_LENGTH];
      OsRng.fill_bytes(&mut buf);

      Uint8Array::from(&buf[..])
    }
}
