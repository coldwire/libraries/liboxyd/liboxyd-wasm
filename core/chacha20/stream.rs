use serde::{Deserialize, Serialize};
use wasm_bindgen::JsValue;
use web_log;

use super::{OutputStream, ReadStream};

use liboxyd::chacha20::{
    chacha20poly1305::{
        aead::{stream, KeyInit},
        XChaCha20Poly1305,
    },
    utils::{CHACHA_KEY_LENGTH, CHACHA_STREAM_NONCE_LENGTH},
};

#[derive(Serialize, Deserialize, Debug)]
struct ReadResult {
    done: bool,
    chunk: Option<i32>,
    value: Option<Vec<u8>>,
}

pub async fn encrypt_stream(
    nonce: &[u8],
    key: &[u8],
    src: ReadStream,
    out: OutputStream,
) -> Result<(), JsValue> {
    if key.len() != CHACHA_KEY_LENGTH {
        Err(format!(
            "(liboxyd): Key size invalid (len={} != {})",
            key.len(),
            CHACHA_KEY_LENGTH
        ))?;
    }

    if nonce.len() != CHACHA_STREAM_NONCE_LENGTH {
        Err(format!(
            "(liboxyd): Nonce size invalid (len={} != {})",
            nonce.len(),
            CHACHA_STREAM_NONCE_LENGTH
        ))?;
    }

    let aead = XChaCha20Poly1305::new(key.as_ref().into());
    let mut stream_encryptor = stream::EncryptorBE32::from_aead(aead, nonce.as_ref().into());

    loop {
        let reader = src.read().await?;

        // Deserialize into ReadResult to get {done, data}
        let reader_result: ReadResult = match serde_wasm_bindgen::from_value(reader) {
            Ok(r) => r,
            Err(e) => Err(format!("(liboxyd): js_read_result error: {:?}", e))?,
        };

        match reader_result.value {
            Some(v) => {
                // Encrypt chunks of a fixed size, if the last one is smaller then it's the end of the file ^^
                if !reader_result.done {
                    web_log::println!(
                        "(liboxyd): Encrypt chunk, len={}, chunk={:?}, done={}",
                        v.len(),
                        reader_result.chunk,
                        reader_result.done
                    );

                    let cipher = stream_encryptor.encrypt_next(v.as_slice());
                    match &cipher {
                        Ok(res) => out.write(&res, reader_result.chunk),
                        Err(err) => Err(format!("(liboxyd): stream encryption error: {:?}", err))?,
                    }
                    .await?;
                } else {
                    if v.len() > 0 {
                        web_log::println!("(liboxyd): Encrypt last chunk, len={}", v.len());

                        let cipher = stream_encryptor.encrypt_last(&v[..v.len()]);
                        match &cipher {
                            Ok(res) => out.write(&res, reader_result.chunk),
                            Err(err) => {
                                Err(format!("(liboxyd): stream encryption error: {:?}", err))?
                            }
                        }
                        .await?;
                    }

                    break;
                }
            }
            None => (),
        }
    }

    // Close stream
    out.close().await?;

    Ok(())
}

pub async fn decrypt_stream(
    nonce: &[u8],
    key: &[u8],
    src: ReadStream,
    out: OutputStream,
) -> Result<(), JsValue> {
    if key.len() != CHACHA_KEY_LENGTH {
        Err(format!(
            "(liboxyd): Key size invalid (len={} != {})",
            key.len(),
            CHACHA_KEY_LENGTH
        ))?;
    }

    if nonce.len() != CHACHA_STREAM_NONCE_LENGTH {
        Err(format!(
            "(liboxyd): Nonce size invalid (len={} != {})",
            nonce.len(),
            CHACHA_STREAM_NONCE_LENGTH
        ))?;
    }

    let aead = XChaCha20Poly1305::new(key.as_ref().into());
    let mut stream_decryptor = stream::DecryptorBE32::from_aead(aead, nonce.as_ref().into());

    loop {
        let reader = src.read().await?;

        // Deserialize into ReadResult to get {done, data}
        let reader_result: ReadResult = match serde_wasm_bindgen::from_value(reader) {
            Ok(r) => r,
            Err(e) => Err(format!("(liboxyd): js_read_result error: {:?}", e))?,
        };

        match reader_result.value {
            Some(v) => {
                // Encrypt chunks of a fixed size, if the last one is smaller then it's the end of the file ^^
                if !reader_result.done {
                    web_log::println!("(liboxyd): Decrypt chunk, len={}", v.len());

                    let cipher = stream_decryptor.decrypt_next(v.as_slice());
                    match &cipher {
                        Ok(res) => out.write(&res, reader_result.chunk),
                        Err(err) => Err(format!("(liboxyd): stream decryption error: {:?}", err))?,
                    }
                    .await?;
                } else {
                    if v.len() > 0 {
                        web_log::println!("(liboxyd): Decrypt last chunk, len={}", v.len());

                        let cipher = stream_decryptor.decrypt_last(&v[..v.len()]);
                        match &cipher {
                            Ok(res) => out.write(&res, reader_result.chunk),
                            Err(err) => {
                                Err(format!("(liboxyd): stream decryption error: {:?}", err))?
                            }
                        }
                        .await?;
                    }

                    break;
                }
            }
            None => (),
        }
    }

    // Close stream
    out.close().await?;

    Ok(())
}
