import { InitOutput } from "../pkg";
import * as services_mod from "./services";
import * as utils_mod from "./utils";
export declare let API_ENDPOINT: string;
export declare const init: (endpoint: string) => Promise<InitOutput>;
export declare const crypto: {
    secp256k1: typeof import("./crypto/secp256k1");
    sha256: typeof import("./crypto/sha256");
    xchacha20: typeof import("./crypto/xchacha20");
};
export declare const utils: typeof utils_mod;
export declare const services: typeof services_mod;
