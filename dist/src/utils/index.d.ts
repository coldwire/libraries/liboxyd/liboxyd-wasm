import * as reader_mod from "./reader";
import * as writer_mod from "./writer";
export declare const base64: {
    encode(data: Uint8Array): string;
    decode(data: string): Uint8Array;
};
export declare const random: (size: number) => Uint8Array;
export declare const reader: typeof reader_mod;
export declare const writer: typeof writer_mod;
